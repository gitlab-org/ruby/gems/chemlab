# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'chemlab/version'

Gem::Specification.new do |spec|
  spec.name = 'chemlab'
  spec.version = Chemlab::VERSION
  spec.authors = ['GitLab Quality']
  spec.email = ['quality@gitlab.com']

  spec.required_ruby_version = '>= 2.5' # rubocop:disable Gemspec/RequiredRubyVersion

  spec.summary = 'Automation framework built by GitLab, for the world.'
  spec.homepage = 'https://about.gitlab.com/'
  spec.metadata = { 'source_code_uri' => 'https://gitlab.com/gitlab-org/quality/chemlab' }
  spec.license = 'MIT'

  spec.files = `git ls-files -- lib/* bin/* Rakefile`.split("\n")

  spec.bindir = 'bin'
  spec.executables << 'chemlab'

  spec.require_paths = ['lib']

  spec.add_development_dependency 'climate_control', '~> 1.0.0'
  spec.add_development_dependency 'nokogiri', '~> 1.10.9'
  spec.add_development_dependency 'pry', '~> 0.14.1'
  spec.add_development_dependency 'rspec', '~> 3.7'
  spec.add_development_dependency 'rspec-parameterized', '~> 0.4.2'
  spec.add_development_dependency 'rubocop', '~> 1.14.0'
  spec.add_development_dependency 'rubocop-rspec', '~> 2.3.0'
  spec.add_development_dependency 'yard', '~> 0.9.26'

  spec.add_runtime_dependency 'colorize', '~> 0.8'
  spec.add_runtime_dependency 'i18n', '~> 1.8'
  spec.add_runtime_dependency 'rake', '>= 12', '< 14'
  spec.add_runtime_dependency 'selenium-webdriver', '>= 3', '< 5'
  spec.add_runtime_dependency 'watir', '>= 6', '< 8'
end
