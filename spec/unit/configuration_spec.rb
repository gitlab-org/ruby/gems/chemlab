# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  RSpec.describe Configuration do
    describe '.add_config' do
      subject(:config) do
        Class.new(described_class) do
          attribute :test_config
        end.new
      end

      it { is_expected.to respond_to(:test_config) }
      it { is_expected.to respond_to(:test_config=) }
    end

    describe '#browser=' do
      subject(:config) do
        described_class.new
      end

      let(:browser) { instance_spy('browser') }
      let(:options) { :chrome }

      before do
        stub_const('Chemlab::Runtime::Browser', browser)
        config.browser = :chrome
      end

      it 'launches a browser' do
        expect(browser).to have_received(:new).with(options)
      end
    end

    describe '#libraries' do
      subject(:config) do
        described_class.new
      end

      it 'defaults to nil' do
        expect(config.libraries).to eq(nil)
      end
    end

    describe '#libraries=' do
      subject(:config) do
        described_class.new
      end

      before do
        Object.const_set('Test', Module.new)
        ::Test.const_set('Page', Module.new)
        config.libraries = [::Test]
      end

      it 'adds the module to the Chemlab::Vendor module' do
        aggregate_failures do
          expect(config.libraries).to be_a(Module)
          expect(Chemlab::Vendor.const_defined?('Test')).to eq(true)
          expect(Chemlab::Vendor::Test.const_defined?('Page')).to eq(true)
        end
      end
    end

    describe 'configs' do
      it { is_expected.to respond_to(:base_url) }
      it { is_expected.to respond_to(:base_url=) }
    end
  end
end
