# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  module CLI
    RSpec.describe Generator do
      describe '.generate' do
        context 'with invalid arguments' do
          it 'raises ArgumentError when insufficient arguments passed' do
            aggregate_failures do
              expect { described_class.generate(nil, nil, nil) }.to raise_error(ArgumentError)
              expect { described_class.generate('foo', nil, 'bar') }.to raise_error(ArgumentError)
              expect { described_class.generate(nil, 'foo', nil) }.to raise_error(ArgumentError)
            end
          end

          it 'raises ArgumentError when a generator does not exist' do
            expect { described_class.generate('invalid', 'Invalid', []) }.to raise_error(ArgumentError)
          end
        end
      end
    end
  end
end
