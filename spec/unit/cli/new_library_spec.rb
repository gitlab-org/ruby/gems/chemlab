# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  module CLI
    RSpec.describe NewLibrary do
      describe '.scaffold' do
        context 'when the directory already exists' do
          let(:dir) { 'library-test' }

          before do
            Dir.mkdir(dir)
          end

          after do
            Dir.delete(dir)
          end

          it 'raises an error' do
            expect do
              described_class.scaffold(dir)
            end.to raise_error(RuntimeError)
          end
        end

        context 'when scaffolding runs' do
          let(:dir) { 'testing' }

          before do
            described_class.scaffold(dir)
          end

          after do
            FileUtils.remove_dir(dir, true)
          end

          it 'creates the directory' do
            expect(Dir).to exist(dir)
          end

          it 'has no occurrences of "new_library"' do
            expect(Dir.glob(File.join(dir, '**', 'new_library*'))).to be_empty
          end

          it 'has no erbs remaining' do
            expect(Dir.glob(File.join(dir, '**', '*.erb'))).to be_empty
          end
        end
      end
    end
  end
end
