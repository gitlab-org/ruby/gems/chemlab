# frozen_string_literal: true

require 'spec_helper'

module Chemlab
  module Runtime
    RSpec.describe Logger do
      subject(:logger) do
        Class.new do
          include Logger
        end.new
      end

      describe '#log' do
        let(:message) { 'test' }
        let(:message_r) { Regexp.new(message) }

        context 'with message formatting' do
          it 'defaults the type to :info and prefix' do
            expect { logger.log(message) }.to output(/INFO\t:: #{message}/).to_stdout
          end

          context 'when specifying a type' do
            it 'outputs error' do
              aggregate_failures do
                expect { logger.log(message, :error) }.to output(/ERRO\t:: #{message}/).to_stderr
                expect { logger.log(message, :err) }.to output(/ERR\t:: #{message}/).to_stderr
              end
            end

            it 'outputs warning' do
              aggregate_failures do
                expect { logger.log(message, :warn) }.to output(/WARN\t:: #{message}/).to_stderr
                expect { logger.log(message, :warning) }.to output(/WARN\t:: #{message}/).to_stderr
              end
            end
          end

          it 'outputs timestamp' do
            expect do
              logger.log(message, :info)
            end.to output(%r{^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} / INFO\t:: #{message}}).to_stdout
          end
        end

        it 'defaults to $stdout' do
          expect { logger.log(message) }.to output(message_r).to_stdout
        end
      end
    end
  end
end
