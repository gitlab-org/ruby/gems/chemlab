# frozen_string_literal: true

module Chemlab
  RSpec.describe 'VERSION' do
    # https://semver.org/
    it 'matches semantic versioning' do
      expect(VERSION).to match(/^\d+\.\d+\.\d$/)
    end
  end
end
