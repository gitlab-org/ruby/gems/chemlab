# frozen_string_literal: true

require 'e2e_helper'

RSpec.describe ExampleApp do
  context 'when navigating to the page library' do
    before do
      ExampleApp::Index.perform(&:visit)
    end

    it 'goes to example' do
      expect(ExampleApp::Index.perform(&:header)).to eq('Example Domain')
    end
  end

  context 'when navigating to a path' do
    before do
      Gitlab::SignIn.perform(&:visit)
    end

    it 'shows the login page' do
      Gitlab::SignIn.perform do |sign_in|
        expect(sign_in).to be_on_page
      end
    end
  end
end
