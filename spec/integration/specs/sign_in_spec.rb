# frozen_string_literal: true

require 'integration_helper'

module Chemlaboratory
  RSpec.shared_examples 'sign in' do
    it 'defaults to not logged in' do
      Page::SignIn.perform do |sign_in|
        expect(sign_in).not_to be_signed_in
      end
    end

    it 'can login' do
      Page::SignIn.perform do |sign_in|
        sign_in.username = 'username'
        sign_in.password = 'password'
        sign_in.sign_in

        expect(sign_in).to be_signed_in
      end
    end
  end

  RSpec.describe 'Sign in' do
    context 'when from sign in page' do
      before do
        Page::SignIn.perform(&:visit)
      end

      it_behaves_like 'sign in'
    end

    context 'when from index page' do
      before do
        Page::Index.perform do |index|
          index.visit
          index.sign_in
        end
      end

      it_behaves_like 'sign in'
    end
  end
end
