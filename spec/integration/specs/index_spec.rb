# frozen_string_literal: true

require 'integration_helper'

module Chemlaboratory
  RSpec.describe 'Index' do
    before do
      Page::Index.perform(&:visit)
    end

    it 'has a header element' do
      Page::Index.perform do |index|
        expect(index.header_title).to eq('Welcome to the Lab.')
      end
    end
  end
end
