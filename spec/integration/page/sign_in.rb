# frozen_string_literal: true

module Chemlaboratory
  module Page
    # Sign in page
    class SignIn < Chemlab::Page
      path '/sign_in.html'

      text_field :username, id: 'username'
      text_field :password
      button :sign_in

      span :signed_in

      # Sign in with username and password
      # @param [String] username
      # @param [String] password
      # @example
      #   Page::SignIn.perform do |sign_in|
      #     sign_in.sign_in_as('user', 'pass')
      #   end
      def sign_in_as(username:, password:)
        self.username = username
        self.password = password
        sign_in
        signed_in?
      end
    end
  end
end
