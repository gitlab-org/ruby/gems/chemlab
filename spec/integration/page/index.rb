# frozen_string_literal: true

module Chemlaboratory
  module Page
    class Index < Chemlab::Page
      path '/index.html'

      h1 :header_title
      link :sign_in

      link :show_dynamic_element

      div :hidden_element, id: 'hidden-element'
    end
  end
end
