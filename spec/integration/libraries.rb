# frozen_string_literal: true

require_relative '../../lib/chemlab'
$LOAD_PATH.unshift(File.expand_path('.'), __dir__)

module Chemlaboratory
  include Chemlab::Library

  module Page
    autoload :Index, 'page/index'
    autoload :SignIn, 'page/sign_in'
  end

  module Flow
    autoload :SignIn, 'flow/sign_in'
  end
end
