# Chemlab

Test Automation Toolkit built by GitLab, for the world.

Powered by Watir.

## Synopsis

Chemlab is a Watir wrapper, a design scaffold and a CLI. It was originally designed to join the existing testing practices
at GitLab from two separate frameworks, the GitLab Capybara Framework, and the CustomersDot Watir framework to one.

## Design Considerations

1. **Based on Watir**: Adopted from the principles of Watir; Using the power of Watir Elements and design principles set
   forth by Watir.
1. **Modular and Extensible**: Chemlab is designed to approach the Page Object Model from a modular standpoint. Each Page
   Library is contained within a Rubygem or SCM repository and able to be downloaded and used from other Chemlab frameworks.
1. **Page Library Design**: 
1. **Testability First**: Approaching Test Automation as it was meant to be. Built by Software Engineers for Software
   Engineers.
1. **Method Stubbing**: Due to Chemlab defining a lot of dynamic methods, it can make it difficult
   to determine what actions can be taken. Method stubbing is a great way to accommodate for this.
1. **Placed in-house**: The page libraries should reside within the source of your project, making it the single source
   of truth.

## Installing

To install Chemlab, use the following command:

```shell
$ gem install chemlab
```

Or add Chemlab to your Gemfile:

```shell
gem 'chemlab'
```

## Writing Page Libraries

See the [Design Scaffolding](doc/design_scaffold.md) document

## Stubbing page libraries

```bash
# Generate method stubs in page directory
$ chemlab generate:stubs [path]
```

## Developing Chemlab

See [Develop](doc/develop.md)

### Run unit specs


### Run integration specs

Integration specs are specs that use Chemlab from an end-to-end test perspective.  It is how other projects
will use Chemlab.

```bash
$ bundle exec rspec spec/integration
```

### Generating Documentation

Once the Page Libraries are [stubbed](#stubbing-page-libraries), you can compile the Ruby docs, then run
a server locally.

```bash
$ bundle exec bin/chemlab generate:stubs spec/integration/page
$ bundle exec yardoc 'spec/integration/page/*' && bundle exec yard server
# the documentation should now reside at http://localhost:8808/
```
