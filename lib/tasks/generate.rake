# frozen_string_literal: true

desc <<-DESCRIPTION
  Generate something

  Arguments: WHAT NAME [ARGS]
  Examples:
    - generate page Login path=/users/sign_in
        Generates a page library
  Notes:
    - [ARGS] will be in form key=value separated by `=`
DESCRIPTION
task :generate do |_, args|
  require_relative '../chemlab/cli/generator'

  raise ArgumentError, 'Please specify what to generate and the name' if args.size < 2

  what = args.shift
  name = args.shift

  Chemlab::CLI::Generator.generate(what, name, args)
end
