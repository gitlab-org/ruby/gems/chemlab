# frozen_string_literal: true

desc <<-DESCRIPTION
  Scaffold a new Chemlab Library

  Arguments: PATH
  Examples:
    - new my-library
      Scaffolds a new Chemlab Library in the `my-library` directory.
      This will fail if the PATH already exists
DESCRIPTION
task 'new' do |_, args|
  require_relative '../chemlab/cli/new_library'

  library = args.first

  warn "NOTE: Only creating #{library}" if args.size > 1
  Chemlab::CLI::NewLibrary.scaffold(library)
end
