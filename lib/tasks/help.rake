# frozen_string_literal: true

require 'colorize'

desc 'Show the help'
task '--help' do
  usage = +%(Usage: #{$PROGRAM_NAME} TASK\n)

  Rake.application.tasks.each do |task|
    usage += %(#{task.name.bold}: #{task.comment}\n)

    next unless task.full_comment && task.full_comment != task.comment

    # standardize and colorize the description
    full_comment = task.full_comment.gsub(/(Arguments|Examples|Notes):/, '\1:'.underline)

    # remove the short description and the newlines
    full_comment = full_comment[task.comment.size + 2..]

    usage += %(#{full_comment}\n\n)
  end

  $stdout.puts usage
end
