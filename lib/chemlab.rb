# frozen_string_literal: true

require 'watir'

# Chemlaboratory
module Chemlab
  autoload :Version, 'chemlab/version'

  # Yields the global configuration to a block.
  # @yield [Configuration] global configuration
  #
  # @example
  #     Chemlab.configure do |config|
  #       config.base_url = 'https://example.com'
  #     end
  def self.configure(&block)
    yield configuration(&block) if block_given?
  end

  # Returns the global [Chemlab::Configuration] object. While
  # you _can_ use this method to access the configuration, the more common
  # convention is to use Chemlab.configure.
  #
  # @example
  #     Chemlab.configuration.something = 1234
  # @see Chemlab.configure
  def self.configuration(&block)
    @configuration ||= Chemlab::Configuration.new(&block)
  end

  autoload :Configuration, 'chemlab/configuration'
  autoload :Attributable, 'chemlab/attributable'

  autoload :Element, 'chemlab/element'
  autoload :Component, 'chemlab/component'
  autoload :Page, 'chemlab/page'
  autoload :Library, 'chemlab/library'

  # Runtime modules
  module Runtime
    autoload :Env, 'chemlab/runtime/env'
    autoload :Browser, 'chemlab/runtime/browser'
    autoload :Logger, 'chemlab/runtime/logger'
  end

  module CLI
    autoload :Generator, 'chemlab/cli/generator'
    autoload :Stubber, 'chemlab/cli/stubber'
    autoload :NewLibrary, 'chemlab/cli/new_library'
  end
end
