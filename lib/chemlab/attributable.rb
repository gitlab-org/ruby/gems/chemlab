# frozen_string_literal: true

module Chemlab
  # Attributable module
  # @example
  #   class MyClass
  #     include Attributable
  #     attribute :url
  #     attribute :id
  #     attribute :name do
  #       'test'
  #     end
  module Attributable
    def self.included(base)
      base.class_eval do
        def self.attribute(name)
          default_value = nil
          default_value = yield if block_given?

          attr_writer name

          define_method(name) do
            instance_variable_get("@#{name}") ||
              instance_variable_set(
                "@#{name}",
                default_value
              )
          end
        end
      end
    end
  end
end
