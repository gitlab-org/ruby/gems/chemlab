# frozen_string_literal: true

require 'erb'
require 'tmpdir'

module Chemlab
  module CLI
    # New Library generator module
    module NewLibrary
      module_function

      # Scaffold a library by name
      # @param [String] library_name the name of the library to scaffold
      def scaffold(library_name)
        if Dir.exist?(library_name)
          raise %(Cannot create new library `#{library_name}` as the directory "#{library_name}" already exists)
        end

        require 'chemlab/core_ext/string/inflections'

        @library = {
          name: library_name,
          classified_name: library_name.tr('-', '_').classify,
          underscored_name: library_name.underscore
        }

        $stdout.print %(Scaffolding new library in "#{library_name}/"...)

        Dir.mktmpdir(library_name) do |dir|
          # copy the fixture folder into the tmp folder and name the directory #{library_name}
          root_dir = File.join(dir, library_name)
          FileUtils.copy_entry(File.expand_path('./fixtures/new_library', __dir__), root_dir)

          # rename all `new_library` references to #{library_name}
          Dir.glob(File.join(root_dir, '**', 'new_library*')) do |file|
            FileUtils.move(file, file.gsub('new_library', library_name))
          end

          Dir["#{root_dir}/**/*.erb"].each do |template|
            File.open(template[0..-5], 'w') do |file|
              file.puts ERB.new(File.read(template), trim_mode: '%<>').result_with_hash({ library: @library })
              File.delete(template)
            end
          end

          FileUtils.move(File.join(dir, library_name), library_name)
        end

        $stdout.print " Done\n"
      end

      # Render a given file
      # @param [File] source the source file
      # @note This method will replace `new_library` with the library
      # @return [String] the destination
      def render_file(source)
        puts "Rendering: #{source}"
        # Dir.mkdir(@tmp_destination)
      end
    end
  end
end
