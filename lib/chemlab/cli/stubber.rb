# frozen_string_literal: true

require_relative '../../chemlab'
require 'erb'

module Chemlab
  module CLI
    # This class is dedicated to stubbing Page Libraries
    class Stubber
      class << self
        def libraries
          @libraries ||= []
        end

        # Generate all stubs in a particular path
        def stub_all(path)
          Array(path).each do |p|
            p = File.expand_path(p)

            PageLibrary.new(p).generate_stub if File.file?(p)
            Dir["#{p}/**/*.rb"].each do |f|
              next if File.basename(f).include?('.stub.')

              PageLibrary.new(f).generate_stub
            end
          end
        end

        # For each page that extends Chemlab::Page, run the stubber
        class Chemlab::Page
          def self.inherited(subclass)
            print subclass

            Stubber.libraries << subclass
          end
        end
      end

      # A Class representation of a Page Library
      class PageLibrary
        attr_accessor :path

        def initialize(path)
          @path = File.absolute_path(path)
        end

        # Generate the stub for a given Page Library
        def generate_stub
          $stdout.print "- #{@path} :\t"

          begin
            load @path

            library = Stubber.libraries.last # last appended library is this Page Library

            require 'chemlab/core_ext/string/inflections'

            stub_path = @path.gsub(@path[@path.rindex('.')..], '.stub.rb')
            File.open(stub_path, 'w') do |stub|
              stub.write(ERB.new(File.read(File.expand_path("#{__dir__}/stub.erb")),
                                 trim_mode: '%<>').result_with_hash({ library: library }))
              Stubber.libraries.pop
            end
          rescue StandardError => e
            # $stderr.print(e.message)
            raise e
          ensure
            $stdout.print "\tDone.\n"
          end
        end
      end
    end
  end
end
