# frozen_string_literal: true

module Chemlab
  # Chemlab Configuration
  class Configuration
    include Runtime::Logger
    include Attributable
    extend Forwardable

    # Chemlab Terminal Banner
    BANNER = <<~'BANNER'

                    ##                                                ##
                   ####                                              ####
                  ######                                            ######
                 #######                                           #######
                 ########                                         #########
                ##########                                        #########
                ##########                                       ###########
               ############                                      ############
              ##########/------------------------------------------\#########
             ##########/  / ___| |__   ___ _ __ ___ | | __ _| |__   \#########
             #########|  | |   | '_ \ / _ \ '_ ` _ \| |/ _` | '_ \   |########
            ##########|  | |___| | | |  __/ | | | | | | (_| | |_) |  |#########
            ###########\  \____|_| |_|\___|_| |_| |_|_|\__,_|_.__/  /###########
           #############\------------------------------------------/#############
          \*********************############################********************/
          \\\********************##########################********************///
         \\\\\\******************#########################*******************//////
        \\\\\\\\******************########################******************////////
        \\\\\\\\\\*****************######################*****************//////////
       \\\\\\\\\\\\****************#####################****************/////////////
      \\\\\\\\\\\\\\\***************####################***************///////////////
      \\\\\\\\\\\\\\\\***************##################**************/////////////////
       \\\\\\\\\\\\\\\\\*************#################**************/////////////////
          \\\\\\\\\\\\\\\*************################************////////////////
             \\\\\\\\\\\\\\************##############************//////////////
               \\\\\\\\\\\\\\**********#############***********/////////////
                  \\\\\\\\\\\\**********############**********////////////
                     \\\\\\\\\\\********###########*********///////////
                        \\\\\\\\\********#########********//////////
                          \\\\\\\\\*******########*******////////
                             \\\\\\\*******######******////////
                                \\\\\\*****######*****//////
                                   \\\\\****####****/////
                                     \\\\***###***////
                                        \\\**##**///
                                           \****/
                                             **
    BANNER

    def initialize
      yield self if block_given?

      log(BANNER, :begn) unless hide_banner
      log(<<-CONF, :conf)

      ==> Base URL: #{base_url}
      ==> Browser: #{browser}
      ==> Libraries: #{libraries}
      CONF
    end

    attribute :base_url
    attribute :hide_banner

    # Delegate `default_timeout` to Watir.default_timeout
    # Chemlab.configuration.default_timeout #=> Watir.default_timeout
    # Chemlab.configuration.default_timeout = Watir.default_timeout = 30
    def_delegators Watir, :default_timeout, :default_timeout=

    attr_reader :browser, :libraries

    # Set the browser and browser arguments Chemlab should use
    def browser=(browser)
      @browser = Runtime::Browser.new(browser)
    end

    # Specify which libraries to load
    def libraries=(libraries = [])
      @libraries = Chemlab.const_set('Vendor', Module.new)

      libraries.each do |library|
        @libraries.const_set(library.to_s, library)
      end
    end

    # Call RSpec.configure for additional configuration
    def configure_rspec
      RSpec.configure do |rspec|
        yield rspec if block_given?

        rspec.after(:each) do
          Chemlab.configuration.browser&.session&.engine&.quit
        end
      end
    end
  end
end
