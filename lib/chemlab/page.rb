# frozen_string_literal: true

module Chemlab
  # Representation of a Page on the UI
  class Page < Component
    attribute :path

    # Visit the given page, specified by +path '/the_path'+
    def visit
      Runtime::Browser.navigate_to(self.class)
    end

    # Check that the current Path segment of the URL matches the Path of the page
    # @return [Boolean] true if the path segment matches
    # @example
    #   class Page
    #     path '/test'
    #   end
    #
    #   Page.perform do |page|
    #     page.visit
    #     expect(page).to be_on_page #=> passes if the path segment of the browser URL matches /test
    #   end
    def on_page?
      URI(Chemlab.configuration.browser.url).path&.match?(self.class.path)
    rescue TypeError
      # it's likely there is no path because of the page using the data protocol e.g.: "data:,"
      false
    end
  end
end
