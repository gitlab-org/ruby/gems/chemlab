# frozen_string_literal: true

require 'forwardable'
require 'chemlab/core_ext/string/root_module'

module Chemlab
  module Runtime
    # The browser configurator
    class Browser
      extend Forwardable
      attr_accessor :session
      attr_reader :browser_options

      def_delegators :session, :url, :refresh, :text, :quit

      def initialize(browser_options)
        @browser_options = browser_options
        @session = Session.new(browser_options)
      end

      # Navigate to a given Page library
      # @param [Class<Chemlab::Page>] page_class the class of the Page to navigate to
      # @example
      #   Given:
      #     module TheLibrary
      #       def self.base_url
      #         'https://example.com'
      #       end
      #       class ThePage < Chemlab::Page
      #         path '/path'
      #       end
      #     end
      #
      #   Chemlab::Runtime::Browser.navigate_to(TheLibrary::ThePage) #=> Navigates to https://example.com/path
      #
      # @example
      #   Given:
      #     Chemlab.configure do |chemlab|
      #       chemlab.base_url = 'https://example.com'
      #     end
      #
      #     class ThePage < Chemlab::Page
      #       path '/'
      #     end
      #
      #   Chemlab::Runtime::Browser.navigate_to(ThePage) #=> Navigates to https://example.com/path
      def self.navigate_to(page_class)
        unless page_class&.name.respond_to?(:root_module) && page_class.name.root_module.respond_to?(:base_url)
          return Chemlab.configuration.browser.navigate_to(Chemlab.configuration.base_url + page_class.path)
        end

        Chemlab.configuration.browser.navigate_to(File.join(page_class.name.root_module.base_url, page_class.path))
      end

      # Navigate to a Path
      # @param [String] path the path to navigate to, relative to the +base_url+
      # @return [String] the path that was navigated to
      # @example
      #   Chemlab.configuration.browser.navigate_to('/path') #=> /path
      def navigate_to(path)
        @session ||= Chemlab.configuration.browser.session
        @session.engine.goto(path.to_s)
      end

      # The options used to create the browser session
      def to_s
        @browser_options.to_s
      end

      # An individual session
      class Session
        extend Forwardable
        attr_reader :engine

        def_delegators :engine, :url, :refresh, :text, :quit

        def initialize(browser)
          @engine = Watir::Browser.new(*browser)

          # @engine.goto(Chemlab.configuration.base_url)
        end

        def save_screenshot(file_name)
          engine.screenshot.save(file_name)
        end

        def wait_until(timeout, message = nil, &block)
          engine.wait_until(timeout: timeout, message: message, &block)
        end

        def wait_while(timeout, message = nil, &block)
          engine.wait_while(timeout: timeout, message: message, &block)
        end
      end
    end
  end
end
