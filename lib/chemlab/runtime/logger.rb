# frozen_string_literal: true

module Chemlab
  module Runtime
    # Logger module
    module Logger
      # Log something to a stream
      # @param [Any] obj Any object that responds to #to_s
      # @param [Symbol] type The type of message logged
      # @param [Hash] args Any additional arguments
      # @example
      #   log('something', :info)                                   #=> logs INFO :: something, to $stdout
      #   log('error', :err)                                        #=> logs ERR  :: error, to $stderr
      #   log('important', :important, stream: $stderr)             #=> logs IMPO :: important, to $stderr
      #   log('to file', :info, stream: File.open('file.txt', 'w')) #=> logs INFO :: to file, to the file.txt file
      def log(obj, type = :info, **args)
        raise ArgumentError, "Cannot log #{obj} as it does not respond to to_s!" unless obj.respond_to?(:to_s)

        args[:stream] = type.match?(/(err(or)?|warn(ing)?)/) ? $stderr : $stdout

        prefix = Time.now.strftime('%Y-%m-%d %H:%M:%S / ')
        prefix << type[0..3].upcase << "\t:: " # we only want the prefix to be four chars long
        args[:stream].puts(prefix << obj.to_s)
      end
    end
  end
end
