# frozen_string_literal: true

require 'forwardable'

module Chemlab
  # The base representation of *any* UI component.
  class Component
    extend SingleForwardable

    def_delegator :browser, :browser
    def_delegators :evaluator, :attribute

    # Perform actions against the given page
    # @example
    #   Component.perform do |component|
    #     component.do_something
    #   end
    # @example
    #   Component.perform(&:do_something)
    # @return The instance of +Component+ used to perform the action
    def self.perform
      yield new if block_given?
    end

    class << self
      # Elements defined on the page
      # @example
      #   +text_field :username, id: 'username'+
      #   Will be in form:
      #     {
      #       type: :text_field,
      #       name: :username,
      #       args: [ { id: 'username' } ]
      #     }
      # @return [Array]
      def public_elements
        @public_elements ||= []
      end

      # rubocop:disable Metrics/BlockLength
      Watir::Container.instance_methods(false).each do |watir_method|
        # For each Watir element (h1, text_field, button, text_field, etc)
        define_method watir_method do |name, *args, &block|
          next if public_instance_methods.include? name

          public_elements << { type: watir_method, name: name, args: args }

          # @return [Watir::Element] the raw Watir element
          define_method("#{name}_element") do |**find_args|
            find_element(watir_method, name, args.first, **find_args, &block)
          end

          # @return [Boolean] true if the element is present
          define_method("#{name}?") do |wait: nil, wait_until: nil|
            public_send("#{name}_element", wait: wait, wait_until: wait_until).present?
          end

          # === GETTER / CLICKER === #
          define_method(name) do |wait: nil, wait_until: :exists?|
            element = public_send("#{name}_element", wait: wait, wait_until: wait_until)
            if Element::CLICKABLES.include? watir_method
              element.wait_until(&:present?).click
            elsif Element::INPUTS.include? watir_method
              element.value
            else
              element.text
            end
          end

          if (Element::SELECTABLES + Element::INPUTS).include?(watir_method)
            # === SETTER === #
            define_method("#{name}=") do |val|
              if Element::SELECTABLES.include? watir_method
                public_send("#{name}_element").select val
              else
                public_send("#{name}_element").set val
              end
            end
          end
        end
      end
      # rubocop:enable Metrics/BlockLength
    end

    # If this component is currently visible
    # @return [Boolean] true if the defined elements in the library are present
    # @note The page presence is determined by the elements defined on the page and their requirement
    def visible?
      missing_elements = [] + self.class.public_elements

      self.class.public_elements.each do |element|
        missing_elements.shift if has_element?(element[:type], element[:name], element[:args].first)
      end

      missing_elements.none?
    end

    # DSL for both components and resources.
    class DSL
      def initialize(base)
        @base = base
      end

      # An attribute to define for a component or resource
      def attribute(name)
        @base.module_eval do
          attr_writer(name)

          default_value = block_given? ? yield : nil

          define_singleton_method(name) do |value = nil|
            instance_variable_get("@#{name}") ||
              instance_variable_set(
                "@#{name}",
                value || default_value
              )
          end
        end
      end
    end

    def self.evaluator
      @evaluator ||= DSL.new(self)
    end
    private_class_method :evaluator

    private

    # Find an element on the page
    # @note this method will wait for the element to appear
    # @see [has_element]
    # @api private
    # @example
    #   #find_element(:text_field, :username) #=>
    def find_element(watir_method, name, locator = nil, **args, &block)
      locator = { css: selector_attribute(name).join(',') } if locator.nil?

      # extract extraneous arguments from the locator and insert them into args
      chemlab_options, locator = extract_chemlab_options(**locator)
      args.merge!(chemlab_options)

      old_timeout = Chemlab.configuration.default_timeout
      args[:wait] ||= old_timeout

      return instance_exec(&block) if block_given?

      element = Chemlab.configuration.browser.session.engine.public_send(watir_method, locator)

      return element if args[:wait_until].nil?

      begin
        # change timeout temporarily to the wait specified
        change_timeout(args[:wait])

        # perform wait(s) on the element
        element.wait_until do |conditions|
          break conditions.public_send(args[:wait_until]) unless args[:wait_until].respond_to?(:each)

          args[:wait_until].each { |condition| conditions.public_send(condition) }
        end

        element
      ensure
        # reset timeout
        change_timeout(old_timeout)
      end
    end

    # Extract Chemlab specific options from an array
    # @param [Array<Hash>] args given arguments
    # @return [Array<Array<Hash>>] first array returned are the chemlab options, second is the resulting array
    # @example
    #   arr = { id: 'test', foo: 'bar', wait: 10 }
    #   a, b = extract_chemlab_options!(**arr)
    #   a #=> { wait: 10 }
    #   b #=> { id: 'test', foo: 'bar' }
    def extract_chemlab_options(**args)
      [args.keys.each_with_object({}) do |arg, chemlab_opts|
        chemlab_opts[arg] = args.delete(arg) if %i[wait wait_until].include?(arg)
      end, args]
    end

    # Set / Reset default Chemlab timeout
    # @param [Integer] timeout the timeout to set (defaults to +Chemlab.configuration.default_timeout)
    # @return [Integer] the new timeout
    def change_timeout(timeout = Chemlab.configuration.default_timeout)
      Chemlab.configuration.default_timeout = timeout
    end

    protected

    # Utility function to convert a snake_case string to
    # kebab-case
    # @param [String] text string to convert
    # @return [String] with underscores converted to hyphens
    def convert_to_kebabcase(text)
      text.to_s.tr('_', '-')
    end

    # Utility function to return array of selector attributes
    # @param [String] name of element to find
    # @return [Array] array of attributes
    def selector_attribute(name)
      [
        %([data-testid="#{name}"]),
        %([data-testid="#{convert_to_kebabcase(name)}"]),
        %([data-qa-selector="#{name}"])
      ]
    end

    # Check existence of an element on the page
    # @note this method will _not_ wait for the element
    # @see [find_element]
    # @api private
    # @example
    #   #has_element?(:text_field, :username) => returns false if the element is is not there
    def has_element?(watir_method, name, locator = nil)
      locator = { css: selector_attribute(name).join(',') } if locator.nil?

      Chemlab.configuration.browser.session.engine.public_send(watir_method, locator).present?
    end
  end
end
